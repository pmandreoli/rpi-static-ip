Role Name
=========

stupid role to set static ip on raspberry 

Requirements
------------

raspberry with raspian installed

Role Variables
--------------

#### raspberry network interface
**rpi_network_interface:** eth0
#### new rpi static ip
**rpi_ip:** 192.168.1.19
#### ip router
**rpi_router_ip:** 192.168.1.1
### dns to use 
**rpi_dns_ips:** [ "208.67.222.222", "208.67.220.220" ] 

Dependencies
------------
rpi with rasbian installed 
reboot the device after the role run 

Example Playbook
----------------
```
---

- name: ansible-playbook to configure home-rasberry
  hosts: home_raspberry
  roles: 
    - role: rpi-static-ip

```

License
-------
GPL-3.0-only

Author Information
------------------
email: pietro.mandreoli@gmail.com
